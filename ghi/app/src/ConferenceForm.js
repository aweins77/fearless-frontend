import React, {useEffect, useState} from 'react';

function ConferenceForm(props){
   const [locations,setLocations]= useState([]);
   const [name, setName] = useState('');
   const [starts, setStart] = useState('');
   const [ends,setEnd] = useState('');
   const [description, setDescription] = useState('');
   const [maxPresentations,setmaxPresentations] = useState('');
   const [maxAttendees, setmaxAttendees] = useState('');
   const [location, setLocation] = useState('');




    const handleNameChange=(event)=>{
        const value=event.target.value;
        setName(value);
    }
    const handleStartChange=(event)=>{
        const value=event.target.value;
        setStart(value);
    }
    const handleEndChange=(event)=>{
        const value=event.target.value;
        setEnd(value);
    }
    const handleDescriptChange=(event)=>{
        const value=event.target.value;
        setDescription(value);
    }
    const handleMaxPresentations=(event)=>{
        const value=event.target.value;
        setmaxPresentations(value);
    }
    const handleMaxAttendees=(event)=>{
        const value=event.target.value;
        setmaxAttendees(value);
    }
    const handleLocationChange=(event)=>{
        const value=event.target.value;
        setLocation(value);
    }

    const handleSubmit= async (event)=>{
        event.preventDefault();
        const data={};

        data.name= name;
        data.starts=starts;
        data.ends=ends;
        data.description=description;
        data.max_presentations=maxPresentations;
        data.max_attendees=maxAttendees;
        data.location=location;

        console.log(data);





       }


    const fetchData= async()=>{
        const url='http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setLocations(data.locations)


    }
}



useEffect(() => {
    fetchData();
}, []);









return(<div className="row">
   <div className="offset-3 col-6">
     <div className="shadow p-4 mt-4">
       <h1>Create a new conference</h1>
       <form id="create-conference-form">
         <div className="form-floating mb-3">
           <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
           <label htmlFor="name">Name</label>
         </div>
         <div className="form-floating mb-3">
           <input onChange={handleStartChange} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control"/>
           <label htmlFor="starts">Start date:</label>
         </div>
         <div className="form-floating mb-3">
           <input onChange={handleEndChange} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control"/>
           <label htmlFor="ends">End date:</label>
         </div>
         <div className="mb-3">
           <label htmlFor="description" className="form-label">Description</label>
           <textarea onChange={handleDescriptChange} className="form-control" id="description" rows="3"></textarea>
         </div>
         <div className="form-floating mb-3">
           <input onChange={handleMaxPresentations} placeholder="Max Presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control"/>
           <label htmlFor="max_presentations">Max Presentations</label>
         </div>
         <div className="form-floating mb-3">
           <input onChange={handleMaxAttendees} placeholder="Max Attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control"/>
           <label htmlFor="max_attendees">Max Attendees</label>
         </div>
         <div className="mb-3">
           <select onChange={handleLocationChange}required id="location" name="location" className="form-select">
             <option  value="">Choose a location</option>
             {locations.map(location => {
                return (
                <option  key={location.id} value={location.id}>
                    {location.name}
                </option>
                );
            })}
            </select>
         </div>
         <button className="btn btn-primary">Create</button>
       </form>
     </div>
   </div>
 </div>
   );

}



export default ConferenceForm;
