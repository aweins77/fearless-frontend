import Nav from "./Nav";
import AttendeesList from "./AttendeesList";
import LocationForm from "./LocationForm";
import ConferenceForm from "./ConferenceForm";
import AttendeeForm from "./AttendeeForm";
import PresentationForm from "./PresentationForm";
import MainPage from "./MainPage";
import {BrowserRouter, Routes, Route,} from "react-router-dom";
import React from 'react'


function App(props) {
  if(props.attendees===undefined){
    return null;
  }
  return (
    <React.StrictMode>
    < BrowserRouter>
    <Nav />
    <div className="container">
      <Routes>
        <Route  path= "/" index element={<MainPage />} />
        {/* <Route path="attendees" element={<AttendeesList attendees={props.attendees} />} /> */}
        <Route path = "locations">
          <Route path = "new" element={<LocationForm/>}/>
        </Route>
        <Route path ="conferences">
           <Route path ="new" element={<ConferenceForm/>} />
        </Route>
        < Route path ="attendees">
           <Route path= "" element={<AttendeesList attendees={props.attendees} />} />
           <Route path="new" element={<AttendeeForm />} />
        </Route>
        <Route path ="presentations">
          <Route path = "new" element={<PresentationForm />}/>
        </Route>

       </Routes>



    {/* <AttendeeForm /> */}
    {/* <LocationForm /> */}
    {/* <AttendeesList  attendees={props.attendees}/> */}
    {/* <ConferenceForm /> */}


    </div>
    </BrowserRouter>
    </React.StrictMode>
  );
}
export default App;
