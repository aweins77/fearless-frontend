import React, {useEffect, useState} from 'react';

function PresentationForm(){
   const [conferences,setConferences] = useState([]);
   const [name, setName] = useState('');
   const [email, setEmail] = useState('');
   const [companyName, setCompanyName]= useState('');
   const [synopsis, setSynopsis] = useState('');
   const [title,setTitle] = useState('');
   const [conference,setConference] = useState('');


  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  }

  const handleEmailChange = (event) => {
    const value = event.target.value;
    setEmail(value);
  }

  const handleCompanyNameChange = (event) => {
    const value = event.target.value;
    setCompanyName(value);
  }

  const handleSynopsisChange = (event) => {
    const value = event.target.value;
    setSynopsis(value);
  }

  const handleTitleChange = (event) => {
    const value = event.target.value;
    setTitle(value);
  }

  const handleConferenceChange = (event) => {
    const value = event.target.value;
    setConference(value);
  }

  const handleSubmit= async(event)=>{
    event.preventDefault();

    const data={};
    data.presenter_name= name
    data.presenter_email=email
    data.company_name =companyName;
    data.synopsis= synopsis;
    data.title=title;
    data.conference=conference;
    console.log(conference)
    console.log(data);



    const locationUrl= `http://localhost:8000${conference}presentations/`;
    const fetchConfig={
      method:"post",
      body:JSON.stringify(data),
      headers:{
        'Content-Type':'application/json',

      },
    };

    const response= await fetch(locationUrl,fetchConfig);
    if(response.ok){
        const newPresentation= await response.json();
        console.log(newPresentation);

        setName('');
        setEmail('');
        setCompanyName('');
        setSynopsis('');
        setTitle('');
        setConference('');

    }
}













  const fetchData = async () => {
    const url = "http://localhost:8000/api/conferences/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();

       setConferences(data.conferences);
    //    const selectTag = document.getElementById("conference");
    //    for (let conference of data.conferences) {
    //    const option = document.createElement("option");
    //    option.value = conference.name;
    //    option.innerHTML = conference.name;
    //    selectTag.appendChild(option);
    //  }
    }
  };

  useEffect(() => {
    fetchData();
  }, []);












    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange}placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control"/>
                <label htmlFor="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEmailChange}placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control"/>
                <label htmlFor="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleCompanyNameChange} placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control"/>
                <label htmlFor="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleTitleChange}placeholder="Title" required type="text" name="title" id="title" className="form-control"/>
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label  htmlFor="synopsis">Synopsis</label>
                <textarea onChange={handleSynopsisChange}  id="synopsis" rows="3" name="synopsis" className="form-control"></textarea>
              </div>
              <div className="mb-3">
                <select onChange={handleConferenceChange} required name="conference" id="conference" className="form-select">
                  <option value="">Choose a conference</option>
                  {conferences.map(conference => {
                  return (
                    <option  key ={conference.id} value={conference.href}>
                    {/* value is what computer reads, state.name is for user throught selecitng and sending  the value physically sends back the href to the dictionary*/}

                    {conference.name}
                    </option>
                   );
                 })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
      );
}

export default PresentationForm;
